<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class CommentFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $paintings = $manager->getRepository(Painting::class)->findAll();
        for ($i = 1; $i <= 1000; $i++) {
            $comment = new Comment();
            $comment->setAuthor($faker->name)
                ->setRating($faker->numberBetween(1, 5))
                ->setComment($faker->paragraph())
                ->setCreatedAt($faker->dateTimeBetween('now'))
                ->setPainting($paintings[$faker->numberBetween(1 ,count($paintings)-1)]);
            $manager->persist($comment);
        }
        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            PaintingFixtures::class
        ];
    }
}
