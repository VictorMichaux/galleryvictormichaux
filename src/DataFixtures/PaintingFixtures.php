<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class PaintingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $categories = $manager->getRepository(Category::class)->findAll();
        $authors = $manager->getRepository(Author::class)->findAll();

        for ($i = 1; $i <= 250; $i++) {
            $painting = new Painting();
            $painting->setTitle(ucfirst($faker->words($faker->numberBetween(3, 5), true)))
                ->setDescription($faker->paragraphs(3, true))
                ->setCreatedAt(\DateTime::createFromFormat('Y-m-d', ($faker->date('Y-m-d', 'now'))))
                ->setImage($faker->numberBetween(1, 50) . '.jpg')
                ->setHeight($faker->numberBetween(0, 300))
                ->setWidth($faker->numberBetween(0, 300))
                ->setIsVisible($faker->boolean(80))
                ->setCategory($categories[$faker->numberBetween(0, count($categories) - 1)])
                ->setAuthor($authors[$faker->numberBetween(0, count($authors) - 1)]);
            $manager->persist($painting);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            AuthorFixtures::class
        ];
    }
}
