<?php

namespace App\Form;

use App\Entity\Painting;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PaintingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre de l\'oeuvre'
            ])
            ->add('author', EntityType::class, [
                'label' => 'Sélectionnez un auteur',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:Author',
                'choice_label' => 'name'
            ])
            ->add('category', EntityType::class, [
                'label' => 'Sélectionnez une catégorie',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:Category',
                'choice_label' => 'name'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'oeuvre'
            ])
            ->add('image', FileType::class, [
                'label' => 'Image de l\'oeuvre',
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Uploadez une image'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10M',
                        'mimeTypes' => ["image/jpg", "image/jpeg", "image/png"],
                        'mimeTypesMessage' => 'Uploadez une image au format jpg ou png',
                    ])
                ],
            ])
            ->add('createdAt', DateType::class, [
                'label' => 'Date de création de l\'oeuvre',
                'years' => range(date('Y'), date('Y') - 1020, -1)
            ])
            ->add('height', NumberType::class, [
                'label' => 'Hauteur de l\'oeuvre en centimètres'
            ])
            ->add('width', NumberType::class, [
                'label' => 'Largeur de l\'oeuvre en centimètres'
            ])
            ->add('isVisible', ChoiceType::class, [
                'label' => 'Visibilité de l\'oeuvre',
                'choices' => ['Visible' => 1, 'Invisible' => 0]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
