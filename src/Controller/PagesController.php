<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param PaintingRepository $repository
     * @return Response
     */
    public function home(PaintingRepository $repository) : Response
    {
        $paintings = $repository->findBy(
            ['isVisible' => true],
            ['title' => 'ASC']
        );
        return $this->render('pages/home.html.twig', ['paintings' => $paintings]);
    }

    /**
     * @Route("/painting/{id}", name="painting")
     * @param int $id
     * @param PaintingRepository $paintingRepository
     * @param CommentRepository $commentRepository
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function post( int $id, PaintingRepository $paintingRepository, CommentRepository $commentRepository, EntityManagerInterface $manager, Request $request) : Response
    {
        $painting = $paintingRepository->find($id);
        $comments = $commentRepository->findBy([
            "painting" => $id
        ]);
        $numberOfComments = count($comments);
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedAt(new \DateTime());
            $comment->setPainting($painting);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash('success', 'Votre commentaire a été enregistré');
            return $this->redirect($request->getUri());
        }
        return $this->render('pages/detail.html.twig', [
            'painting' => $painting,
            'comments' => $comments,
            'numberOfComments' => $numberOfComments,
            'form' => $form->createView()
            ]);
    }

    /**
     * @Route("/gallery", name="gallery")
     * @param PaintingRepository $repository
     * @return Response
     */
    public function gallery(PaintingRepository $repository) : Response
    {
        $paintings = $repository->findBy(
            ['isVisible' => true],
            ['title' => 'ASC']
        );
        $numberOfPaintings = count($paintings);
        return $this->render('pages/gallery.html.twig', [
            'paintings' => $paintings,
            'numberOfPaintings' => $numberOfPaintings
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('pages/about.html.twig');
    }

    /**
     * @Route("/team", name="team")
     */
    public function team()
    {
        return $this->render('pages/team.html.twig');
    }
}
