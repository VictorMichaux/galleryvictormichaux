<?php

namespace App\Controller;

use App\Entity\Painting;
use App\Form\PaintingType;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @param PaintingRepository $repository
     * @return Response
     */
    public function index(PaintingRepository $repository): Response
    {
        $paintings = $repository->findBy(
            [],
            ['title' => 'ASC']
        );
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'paintings' => $paintings
        ]);
    }

    /**
     * @Route("/addpainting", name="addPainting")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function addPainting(Request $request, EntityManagerInterface $manager, SluggerInterface $slugger): Response
    {
        $painting = new Painting;
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();
                // Move the file to the directory where brochures are stored
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $painting->setImage($newFilename);
            }
            else {
                $painting->setImage('default.jpg');
            }
            // ... persist the $product variable or any other work
            $manager->persist($painting);
            $manager->flush();
            $this->addFlash('success', 'Votre nouvelle oeuvre a été enregistrée');
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/addPainting.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delpainting/{id}", name="delPainting")
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delPainting(Painting $painting, EntityManagerInterface $manager): Response
    {
        $manager->remove($painting);
        $manager->flush();
        $this->addFlash('warning', 'Cette oeuvre a été supprimée');
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/editPainting/{id}", name="editPainting")
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return Response
     */
    public function editPainting(Painting $painting, EntityManagerInterface $manager, Request $request, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $painting->setImage($newFilename);
            }
            $manager->persist($painting);
            $manager->flush();
            $this->addFlash('success', 'Vos modifications ont été enregistrées');
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/editPainting.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/paintingVisibility/{id}", name="paintingVisibility")
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public
    function paintingVisibility(Painting $painting, EntityManagerInterface $manager): Response
    {
        if ($painting->getIsVisible() == 1) {
            $painting->setIsVisible(0);
        } else {
            $painting->setIsVisible(1);
        }
        $manager->persist($painting);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }
}


