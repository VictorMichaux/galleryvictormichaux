Mode d'emploi:

- Installer Composer

`composer install`

- Créer la DB 'galleryVictorMichaux'

`php bin/console doctrine:database:create`

- Migrer les tables (la migration est déjà présente dans le dossier migrations)

`php bin/console doctrine:migrations:migrate`

- Loader les fixtures

`php bin/console doctrine:fixtures:load`

- Pour uploader des fichiers de plus de 2M,ne pas oublier de changer dans php.ini

`post_max_size = 16M` et `upload_max_filesize = 16M`

(On peut metttre la limite qu'on veut mais j'ai choisi 16M)